import numpy as np
import cv2
import argparse
#####Importar imagen####
face_cascade = cv2.CascadeClassifier('C:\\Users\\JAIME PINEDA\\Desktop\\final\\haarcascade_frontalcatface.xml')
cap = cv2.imread('C:\\Users\\JAIME PINEDA\\Desktop\\final\\original.jpg')
#### Reconocimiento de bordes####
contorno = face_cascade.detectMultiScale(cap, 1.3, 5)
blur=cv2.GaussianBlur(cap, (11,11),0)
borde = cv2.Canny(blur, 10, 30)
(cnts, _) = cv2.findContours(borde.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
####Cambio de Color####
cv2.imwrite('bordeimg.jpg',borde)
cap2 = cv2.imread('C:\\Users\\JAIME PINEDA\\Desktop\\final\\bordeimg.jpg')
cambio= cv2.cvtColor(cap2, cv2.COLOR_BGR2HSV)
####Mostramos la imagen####
cv2.imshow('original',cap)
cv2.imshow('borde',borde)
cv2.imshow('OTRO COLOR',cambio)
cv2.waitKey(0)
